#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc,char *argv[]) {
	char *arg = argv[1];
	pid_t pid;

	if(argc!=2) {
		printf("Podaj nazwe potoku jako argument\n");
		exit(EXIT_FAILURE);
	}
	if(mkfifo(arg,0777) == -1) {
		perror("mkfifo error");
		exit(EXIT_FAILURE);
	}
	switch(pid = fork()) {
		case -1:
			perror("fork error");
			exit(EXIT_FAILURE);
		case 0:
			if(execl("./producent.x","./producent.x",arg, NULL) == -1) {
				perror("execl error");
				exit(EXIT_FAILURE);
			}
		default:
			break;
	}
	switch(pid = fork()) {
		case -1:
			perror("fork error ");
			exit(EXIT_FAILURE);
		case 0:
			if(execl("./konsument.x","./konsument.x",arg, NULL) == -1) {
				perror("execl error");
				exit(EXIT_FAILURE);
			}
		default:
			break;
	}
	wait(0);
	wait(0);
	unlink(arg);
	return EXIT_SUCCESS;
}
