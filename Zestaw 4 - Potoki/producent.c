#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

char buf[200];
int time = 20;

int main(int argc, char * argv[]) {
     FILE *fp;
     int fd;
     char *arg = argv[1];
     
     if(argc != 2) {
          perror("Zla ilosc argumentow!");
          exit(EXIT_FAILURE);
     }
     
     fp = fopen("./magazyn.txt", "r");
     fd = open(arg, O_WRONLY);

     while(!feof(fp)) {
          usleep(rand()%time);
          if(fgets(buf, 200, fp) == buf) {
               if(write(fd, buf, 200) == -1) {
                    perror("Write error");
                    exit(EXIT_FAILURE);
               }
          }
     }
     if(fclose(fp) == EOF) {
          perror("fclose error");
          exit(EXIT_FAILURE);
     }
     if(close(fd) == -1) {
         perror("close error");
         exit(EXIT_FAILURE);
     }
     return EXIT_SUCCESS;
}