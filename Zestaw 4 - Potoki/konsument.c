#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h> 
#include <signal.h>
#define SIZE 2048

int main(int argc,char *argv[]) {
	int file, storage;
	char *buf;
	
	file = open(argv[1],O_RDONLY);
	storage = open("schowek.txt", O_WRONLY | O_CREAT, 0777);
	buf = malloc(SIZE);

	if(storage == -1) {
		perror("save error");
		exit(EXIT_FAILURE);
	}
	if(file == -1) {
		perror("fd read error");
		exit(EXIT_FAILURE);
	}

	while(read(file,buf,SIZE)>0) {
		if(write(storage,buf,strlen(buf)) == -1) {
			perror("storage save error");
			exit(EXIT_FAILURE);
		}
		printf("przekazuje: %s\n",buf);
	}	
	close(file);
	close(storage);
	return EXIT_SUCCESS;
}
