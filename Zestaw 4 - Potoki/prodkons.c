#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>    
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

char buffer[20];
int microseconds;
FILE *fp;

int Konsument(int file[]) {
    ssize_t fread;
    close(file[1]);

    fp = fopen("./schowek.txt", "w");
    while((fread = read(file[0], &buffer, 20)) > 0) {
         usleep(rand() % microseconds);
         fputs(buffer, fp);
    }
    if(fread == -1) {
         perror("Error read");
         exit(EXIT_FAILURE);
    }

    if(fclose(fp) == EOF) {
         perror("Error -  fclose konsument");
         exit(EXIT_FAILURE);
    }
    if(close(file[0]) == -1) {
         perror("Error - close konsument");
    }
    return 0;
}


int Producent(int file[]) {
     close(file[0]);
     fp = fopen("./magazyn.txt", "r");
     while(!feof(fp)) {
          usleep(rand()%microseconds);
          if(fgets(buffer, 20, fp) == buffer) {
               if(write(file[1], &buffer, 20) == -1) {
                    perror("Write error");
                    exit(EXIT_FAILURE);
               }
          }
     }
     if(close(file[1]) == -1) {
         perror("Error - close producent");
         exit(EXIT_FAILURE);
    }
     if(fclose(fp) == EOF) {
          perror("Error - fclose producent");
          exit(EXIT_FAILURE);
     }
     return 0;
}

int main(int argc, char * argv[]) {
     int file[2];
     int dpipe;
     microseconds = rand() % (100000 + 1 - 0) + 10000; //random liczba

     if(argc != 1) {
          perror("Error - zla ilosc arg");
          exit(EXIT_FAILURE);
     }
     dpipe = pipe(file);

     if(dpipe == -1) {
          perror("Pipe error");
          exit(EXIT_FAILURE);
     }

     switch(fork()) {
          case -1:
               perror("Error - tworzenie potomnego procesu");
               exit(EXIT_FAILURE);
               break;
          case 0:
               Konsument(file);
               break;
          default:
               Producent(file);
               wait(0);
               break;
          }
     return 0;
}