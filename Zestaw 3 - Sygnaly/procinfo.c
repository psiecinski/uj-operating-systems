#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int procinfo(const char* name){
  pid_t pid = getpid();
  printf("Id procesów : GID- %d, UID- %d, PID- %d, PPID-%d, PGID- %d\n", getgid(), getuid(), getpid(), getppid(), getpgid(pid)); //wypisanie id_procesow
  return 0;
}