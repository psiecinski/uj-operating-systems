#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include "procinfo.h"

int groupHandler(char *zero, char* first, char *second, int sign) {
	int pid;
	procinfo(zero);
	switch (pid = fork ()) {
		case -1:
			perror("Nie udalo sie utworzyc procesu potomnego.");
			exit(EXIT_FAILURE);
		case 0:
		    procinfo(zero);
			setpgid(0,0); //ustawiamy liderem
			int pid2;
			for(int i=0; i<3; i++){
				switch(pid2 = fork()){
					case -1: 
						perror ("Nie udalo sie utworzyc procesu potomnego.");
						exit(EXIT_FAILURE);
					case 0:
						execl("obsluga.x", "obsluga.x", first, second, NULL);
					default:
						signal(sign, SIG_IGN);
				}
			}
			for(int i=0; i<3; i++){
				pid_t tid = wait(NULL); //pid_t wait(int *status)
				printf("Status zakonczenia: %d\n", tid);
			}
		default:
			return pid;	
	}
}

int main(int argc, char *argv[]) {

	if (argc != 3) { //sprawdzam czy liczba argumentów podanych przez uzytkownika sie zgadza
		perror ("Nieprawidlowa liczba argumentow.");
		exit(EXIT_FAILURE);
	}
	char *zero = argv[0];
	char *first = argv[1];
	char *second = argv[2];
	int signal = atoi(argv[2]);
	int pid;
	pid = groupHandler(zero, first, second, signal);
	sleep(1);

	if (kill(-getpgid(pid), 0) == -1){ 
		perror("Nie da sie wyslac sygnalu"); 
		exit(EXIT_FAILURE);
	}

	if(kill(-getpgid(pid), signal) == -1) { 
		printf("blad podczas wysylania"); 
		exit(EXIT_FAILURE); 
	}

	exit(EXIT_SUCCESS); 
	return 0;
}




