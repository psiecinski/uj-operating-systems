#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include "procinfo.h"

void handler (int sign) {
	printf ("Otrzymalem dany sygnal: %d.\n", sign);
}

int main (int argc, char *argv[])
{
	if (argc != 3) {
        printf("Wprowadzono nieprawidlowa liczbe argumentow.");
		exit (EXIT_FAILURE);
	}
	int sign = -1;
	if (argc == 2) {
		pause ();
	}
	else {
		sign = atoi(argv[2]);
	}
    procinfo(argv[0]);
    switch(*argv[1]){
        case 'd' :
            if (signal (sign, SIG_DFL) == SIG_ERR ) {
                perror ("SIG_DFL error");
                exit (EXIT_FAILURE);
            } else {
                signal(sign, SIG_DFL);
                pause();
                exit(EXIT_SUCCESS);
            }
        case 'i' :
            if ( signal (sign, SIG_IGN) == SIG_ERR ) {
			    perror ("SIG_IGN error");
			    exit (EXIT_FAILURE);
		    } else {
                signal(sign, SIG_IGN);
                pause();
                exit(EXIT_SUCCESS);
            }
        case 'p' :
            if ( signal (sign, handler) == SIG_ERR ) {
			        perror ("handlererror ");
			        exit (EXIT_FAILURE);
		    } else {
                signal(sign, handler);
                pause();
                exit(EXIT_SUCCESS);
            }
        default : 
            perror ("Wprowadzono nieprawidlowy pierwszy argument. Prawidlowe wartosci: d, i lub p.");
		    exit (EXIT_FAILURE);
    }
	return EXIT_SUCCESS;
}
