#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include "procinfo.h"

int main(int argc, char *argv[]) {
	if (argc != 3) {
		perror("Nieprawidlowa liczba argumentow.");
        return 0;
	}
	char *run = argv[1];
	int signal = atoi (argv[2]);
	int pid;

	switch (pid = fork ()) {
		case -1:
			perror("Tworzenie procesu potomnego nie powiodlo sie.");
		case 0:
			execl("obsluga.x", "obsluga.x", run, (char*) NULL);
				
		default:
			if(kill(pid,0) == -1){
				perror("Brak mozliwosci wyslania sygnalu - brak dziecka");
				exit(EXIT_FAILURE);
			}
			if(kill(pid,signal) == -1){
				printf("Blad wyslania sygnalu");
				exit(EXIT_FAILURE);
			} else {
				printf ("Wysylam sygnal %d do procesu o id: %d \n", signal, pid);
				exit(EXIT_SUCCESS);
			}
	}
	return 0;
}